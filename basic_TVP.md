For 4 persons (as TVP is 52% Protein), in a cooking pot:

Quantity | Value | Ingredient | Extra Info
-|-|-|-
1 | Tea | Marmite / Vegemite | Meaty flavour, Marmite and low-salt Vegemite have B-12
0.5 | Tea | MSG | Make Shit Good
0.5 | Tea | Coconut Oil | The closest vegetable fat to meat fat
1 | Table | Soy Sauce | Colour, salt and some extra umami
0.5 | Table | Mirin / White Wine | Quicker cooking time, with extra flavouring (and some nice sweetness if Mirin is used)
1 | Tea | Corn Starch | Browning and fond
0.5 | Cup | TVP (Soy) | The base ingredient, can also be pea protein

Cook on <ins>medium</ins> and lower to <ins>low</ins> heat when bubbling. When no liquid is left, it is done.
