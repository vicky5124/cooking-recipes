For 200 g of Tofu

### Dry

Quantity | Value | Ingredient | Extra Info
-|-|-|-
1 | Tea | Garlic Powder |
0.5 | Tea | Freshly ground Pepper | Can be pre-ground white pepper
1.5 | Tea | Paprika | Can be spicy
1 | Tea | Onion Powder |
2 | Tea | Chiken Seasoning | Condis
0.5 | Tea | Salt |
2 | Tea | Corn Starch |
1 | Tea | MSG | Make Shit Good

### Thick Sauce

Quantity | Value | Ingredient | Extra Info
-|-|-|-
1 | Tea | Dark Soy Sauce |
1 | Tea | Light Soy Sauce | Normal soy sauce
0.5 | Tea | Rice Vinegar |
15 | Gram | Ketchup | 1 McDonals Packet
2 | Tea | Brown Sugar |
0.5 | Tea | Toasted Sesame Oil |
1 | Tea | Corn Starch |
1 | Tea | MSG | Make Shit Good

### Other Ingredients

- 1/2 Big Onion
- 4 Garlic Cloves
- 1/2 Bell Pepper

## Recipe

1. Cube the tofu, and cover with the dry mix.
2. In a wok or cast iron pan, put the tofu cubes, and fill with neutral oil until the <ins>half way point of the cubes</ins>.
3. Cook in <ins>low</ins> heat until lightly brown, and flip to cook the other side of the cubes.
4. (Optionally) <ins>char the bell pepper</ins> skin with a torch or open flame and remove it.
5. While 2 and 3 are going, <ins>dice\* the onion and bell pepper, and mince\* the garlic</ins> as much as possible.
6. In the same wok or cast iron pan where you did the tofu, clear most of the oil, and stir fry the <ins>onion and pepper</ins> until they slightly char.
7. When chared, throw the <ins>garlic and tofu</ins>, and heat them for <ins>30 seconds</ins>.
8. If wok, lower the heat to low, if cast iron, kill the heat, and <ins>poar in the sauce</ins>.
9. Serve on top of 2 bowls of white rice.

#### Notes

- The sugar can be subsituted with sweet chilly if you want it spicy.
- You can lower the heat by putting less pepper or substitute with white pepper in the spices.

*Dice and Mince:
![Fig.1](./img/Onion Cut Names.jpg)
